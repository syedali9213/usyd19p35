I've attached a modified version of createReport. It doesn't work yet.

Reasons for changing the createReport:
- Current version uses a 2d array. Has an O(n^2) complexity, will get stuck the larger the page becomes
- Current version uses too few colors. Won't work well for scrolling maps
- h337's code is much simpler. Simply define the data point and then append it to the heatmap


I'm looking for a way to allow the user to preview the heatmap, and then be able to download it.
This way we can show the user what to expect as the output.