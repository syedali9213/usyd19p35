//get all the data we've collected from chrome local storage
chrome.storage.local.get(["mouseMovementArray"], function(result) {
  //return if we don't have any data yet
  if (!result.mouseMovementArray || result.mouseMovementArray.length === 0) {
    chrome.runtime.sendMessage({ createdReport: true });
    return;
  }

  // NEW CODE
  // Create canvas
  var Canvas = document.createElement("canvas");
  Canvas.width = window.innerWidth;
  Canvas.height = window.innerHeight;
  // create configuration object
  var config = {
    container: document.getElementById('canvas'),
    radius: 50,
  };
  // create heatmap with configuration
  var heatmapInstance = h337.create(config);

  //Collect data and add to heatmap
  var rawData = result.mouseMovementArray;
  var dataPoint;
  for (let i = 0; i < rawData.length; i++) {
    dataPoint = {
      x: rawData[i][0],
      y: rawData[i][1],
      value: 1
    };
    heatmapInstance.addData(dataPoint);
  }

  //transform data to png image and start downloading
  var download = function() {
    var link = document.createElement("a");
    link.href = document.getElementById("canvas").toDataURL();
    link.download = "TMB_Analyzation.png";
    //link.href = heatmapInstance.getDataURL("image/png");
    link.click();
  };
  download();
  //tell popup.js report has been created
  chrome.runtime.sendMessage({ createdReport: true });
});