const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');

const {startDatabase} = require('./database/mongo');
const {insertData, getData} = require('./database/data');

const app = express();

const ads = [
    {title: 'Hello, world (again)!'}
]

// adding Helmet to enhance your API's security
app.use(helmet());
// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());
// enabling CORS for all requests
app.use(cors());
// adding morgan to log HTTP requests
app.use(morgan('combined'));

//define endpoint
app.get('/', async (req, res) => {
    res.send(await getData());
})

//endpoint to add a new add. Each request has keys id and data
app.post('/', async (req, res) => {
    const newData = req.body;
    console.log('new hit:', req.body)
    for(i in newData["data"]){
        await insertData(i, newData["data"][i], newData['id'])
    }
    // await insertData(newData);
    res.send({ message: 'New data inserted.' })
});

// app.delete('/:id', async (req, res) => {
//     await deleteAd(req.params.id);
//     res.send({ message: 'Ad removed.' });
// });

// endpoint to update data
// app.put('/:id', async (req, res) => {
//     const updatedAd = req.body;
//     await updateAd(req.params.id, updatedAd);
//     res.send({ message: 'Ad updated.' });
// });

startDatabase().then(async () => {
    // await insertAd({title: 'Hello, now from the in-memory database!'});
  
    // start the server
    app.listen(3000, async () => {
      console.log('listening on port 3000');
    });
  });