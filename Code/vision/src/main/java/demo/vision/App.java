package demo.vision;

import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.EntityAnnotation;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


public class App 
{
    public static void main( String[] args ) throws IOException
    {
    	try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {

    	      // The path to the image file to annotate
    		  String baseURL = App.class.getClassLoader().getResource("").toString();
    	      String fileName = baseURL+"find.png";
    	      // Reads the image file into memory
    	      Path path = Paths.get(fileName.substring(6));
    	      byte[] data = Files.readAllBytes(path);
    	      ByteString imgBytes = ByteString.copyFrom(data);
    	      System.out.println("?????????????sdfsdfaadfsfasd的防守犯规??");
    	      // Builds the image annotation request
    	      List<AnnotateImageRequest> requests = new ArrayList<>();
    	      Image img = Image.newBuilder().setContent(imgBytes).build();
    	      Feature feat = Feature.newBuilder().setType(Type.LABEL_DETECTION).build();
    	      AnnotateImageRequest request = AnnotateImageRequest.newBuilder()
    	          .addFeatures(feat)
    	          .setImage(img)
    	          .build();
    	      requests.add(request);

    	      System.out.println("?????????????sdfsdfaadfsfasd??");
    	      // Performs label detection on the image file
    	      BatchAnnotateImagesResponse response = vision.batchAnnotateImages(requests);
    	      System.out.println("?????????????sdfsdfa阿迪达斯发大水adfsfasd??");
    	      List<AnnotateImageResponse> responses = response.getResponsesList();

    	      System.out.println("???????????????");
    	      
    	      for (AnnotateImageResponse res : responses) {
    	        if (res.hasError()) {
    	          System.out.printf("Error: %s\n", res.getError().getMessage());
    	          return;
    	        }
    	        
    	        for (EntityAnnotation annotation : res.getLabelAnnotationsList()) {
    	          annotation.getAllFields().forEach((k, v) ->
    	              System.out.printf("%s : %s\n", k, v.toString()));
    	        }
    	      }
    	    }
    }
}
