﻿window.onload=function(){
	var element = window.document.body;
	var startTime;
	var oldTarget = "nil";
	var timeLength;
	window.document.body.onmouseover = function(e){
  		element = e.target;
		var tagType = e.target.tagName;
		var url = e.target.src;
		
		if(oldTarget!="nil"){
				if(oldTarget!=e.target){
					var endTime =  new Date().getTime();
					timeLength = endTime - startTime;
					console.log('Current staying time is',timeLength);
					ajax({ 
						  type:"POST", 
						  url:"https://vision.googleapis.com/v1/images:annotate?key=YOUR_API_KEY", 
						  dataType:"json", 
						  data:{"val1":"abc","val2":123,"val3":"456"}, 
						  beforeSend:function(){ 
							
						  }, 
						  success:function(msg){ 
							console.log(msg) 
						  }, 
						  error:function(){ 
							console.log("error") 
						  } 
					});
					return;
				}
		}	
		
		if(tagType == "IMG"){
			oldTarget = e.target;
			startTime = new Date().getTime();
			console.log('Current mouse is on', url, 'element');
		}
		
		
		
	}	
}

function convertImgToBase64(url, callback, outputFormat){
       var canvas = document.createElement('CANVAS'),
    　　ctx = canvas.getContext('2d'),
    　　img = new Image;
    　　img.crossOrigin = 'Anonymous';
    　　img.onload = function(){
        　　canvas.height = img.height;
        　　canvas.width = img.width;
        　　ctx.drawImage(img,0,0);
        　　var dataURL = canvas.toDataURL(outputFormat || 'image/png');
        　　callback.call(this, dataURL);
        　　canvas = null; 
        };
    　　img.src = url;
}

function getBase64(img){
        function getBase64Image(img,width,height) {//Pass in specific pixel values when calling width、height to control size, if not use default image size
          var canvas = document.createElement("canvas");
          canvas.width = width ? width : img.width;
          canvas.height = height ? height : img.height;
 
          var ctx = canvas.getContext("2d");
          ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
          var dataURL = canvas.toDataURL();
          return dataURL;
        }
        var image = new Image();
        image.crossOrigin = '';
        image.src = img;
        var deferred=$.Deferred();
        if(img){
          image.onload =function (){
            deferred.resolve(getBase64Image(image));//pass base64 to done upload
          }
          return deferred.promise();//return sessionStorage['imgTest'] after onload done
        }
}

function ajax(){ 
  var ajaxData = { 
    type:arguments[0].type || "GET", 
    url:arguments[0].url || "", 
    async:arguments[0].async || "true", 
    data:arguments[0].data || null, 
    dataType:arguments[0].dataType || "text", 
    contentType:arguments[0].contentType || "application/x-www-form-urlencoded", 
    beforeSend:arguments[0].beforeSend || function(){}, 
    success:arguments[0].success || function(){}, 
    error:arguments[0].error || function(){} 
  } 
  ajaxData.beforeSend() 
  var xhr = createxmlHttpRequest();  
  xhr.responseType=ajaxData.dataType; 
  xhr.open(ajaxData.type,ajaxData.url,ajaxData.async);  
  xhr.setRequestHeader("Content-Type",ajaxData.contentType);  
  xhr.send(convertData(ajaxData.data));  
  xhr.onreadystatechange = function() {  
    if (xhr.readyState == 4) {  
      if(xhr.status == 200){ 
        ajaxData.success(xhr.response) 
      }else{ 
        ajaxData.error() 
      }  
    } 
  }  
} 
  
function createxmlHttpRequest() {  
  if (window.ActiveXObject) {  
    return new ActiveXObject("Microsoft.XMLHTTP");  
  } else if (window.XMLHttpRequest) {  
    return new XMLHttpRequest();  
  }  
} 
  
function convertData(data){ 
  if( typeof data === 'object' ){ 
    var convertResult = "" ;  
    for(var c in data){  
      convertResult+= c + "=" + data[c] + "&";  
    }  
    convertResult=convertResult.substring(0,convertResult.length-1) 
    return convertResult; 
  }else{ 
    return data; 
  } 
}