//get all the data we've collected from chrome local storage
chrome.storage.local.get(["mouseMovementArray"], function(result) {
  //return if we don't have any data yet
  if (!result.mouseMovementArray || result.mouseMovementArray.length === 0) {
    chrome.runtime.sendMessage({ createdReport: true });
    return;
  }
  //create a two dimensional array to store how many times each pixel has been touched by mouse
  //since we use pixel as unit, this array will have a length of "screen height",
  //each subarray in it will have a length of " screen width"
  var lineNumber = window.innerHeight;
  var rowNumber = window.innerWidth;
  var arrangedData = new Array(rowNumber);
  for (let i = 0; i < arrangedData.length; i++) {
    arrangedData[i] = new Array(lineNumber);
  }
  //initialize all values in this array to be 0
  for (let i = 0; i < arrangedData.length; i++) {
    for (let j = 0; j < arrangedData[i].length; j++) {
      arrangedData[i][j] = 0;
    }
  }
  //traverse mouse position array we've collected,
  //find its position in the array created above(use i as positionX and j as positionY) and increase count number by 1
  var rawData = result.mouseMovementArray;
  for (let i = 0; i < rawData.length; i++) {
    arrangedData[rawData[i][0]][rawData[i][1]]++;
  }
  //now the arragedData array contains information of which point has been touched how many times,
  //traverse this array, find the max touched times in order to calculate a relative alpha value
  //e.g, position (100,100) has been touched 100 times
  var max = 0;
  for (let i = 0; i < arrangedData.length; i++) {
    for (let j = 0; j < arrangedData[i].length; j++) {
      if (arrangedData[i][j] > max) {
        max = arrangedData[i][j];
      }
    }
  }
  //prepare two canvas,
  //alphaCanvas is used to generate a Grayscale image,
  //outputCanvas is for colored heatmap after we've finished drawing on alphaCanvas
  //each canvas has a height and width equal to the screen's
  var alphaCanvas = document.createElement("canvas");
  var outputCanvas = document.createElement("canvas");
  alphaCanvas.width = window.innerWidth;
  alphaCanvas.height = window.innerHeight;
  outputCanvas.width = window.innerWidth;
  outputCanvas.height = window.innerHeight;

  //create a context since we are going to draw 2 dimentional shapes
  var ctxAlpha = alphaCanvas.getContext("2d");
  //traverse the array we got from above
  for (let i = 0; i < arrangedData.length; i++) {
    for (let j = 0; j < arrangedData[i].length; j++) {
      //return if this pixel has not been touched at all
      if (arrangedData[i][j] === 0) {
        continue;
      }
      //start drawing
      ctxAlpha.beginPath();
      //draw a circle, center position is (i,j),radius is 50, start angle 0, end angle 2PI, counterclockwise
      ctxAlpha.arc(i, j, 50, 0, Math.PI * 2, true);
      //will fill the circle with a radialGradient, both starting and ending circle has a center posiiton of (i,j), radius are set to 5 and 50, respectively
      let grd = ctxAlpha.createRadialGradient(i, j, 5, i, j, 50);
      //calculate alpha value for each point based on max value we got above
      let alpha = arrangedData[i][j] / max;
      //transform alpha value valid for rgba calculation
      alpha = alpha.toFixed(2);
      //0 stands for center of the circle, color will be the darkest, use the alpha value we've calculated
      //1 stands for outside border of the circle, color will be transparent
      grd.addColorStop(0, `rgba(0,0,0,${alpha})`);
      grd.addColorStop(1, `rgba(0,0,0,0)`);
      //set fill style to the radialGradient
      ctxAlpha.fillStyle = grd;
      //fill the circle
      ctxAlpha.fill();
    }
  }

  //colorize the result
  //create another 2d context on outputCanvas
  var ctxOutput = outputCanvas.getContext("2d");
  for (let i = 0; i < arrangedData.length; i++) {
    for (let j = 0; j < arrangedData[i].length; j++) {
      //traverse alphaCanvas we got
      //for each pixel, get is image data
      //result will be like [0,0,0,100]
      //number at position [3](e.g. 100 above) is the alpha value we needed
      var p = ctxAlpha.getImageData(i, j, 1, 1).data;
      if (p[3] === 0) {
        //this suggest this pixel has not been touched before, so we leave it as blank
        continue;
      }
      //choose a palette for heatmap
      let rgbaValue;
      //below numbers like 15, 30, 45 are used to set threshold for coloring
      //max alpha value is 255
      //numbers I chose are to the lower range to better visualise less active area
      if (p[3] < 15) {
        rgbaValue = `rgba(0,120,176,0.1)`;
      } else if (p[3] < 30) {
        rgbaValue = `rgba(47,180,148,0.2)`;
      } else if (p[3] < 45) {
        rgbaValue = `rgba(137,206,143,0.3)`;
      } else if (p[3] < 60) {
        rgbaValue = `rgba(203,229,124,0.4)`;
      } else if (p[3] < 75) {
        rgbaValue = `rgba(237,205,110,0.5)`;
      } else if (p[3] < 90) {
        rgbaValue = `rgba(244,154,65,0.6)`;
      } else if (p[3] < 105) {
        rgbaValue = `rgba(241,84,29,0.7)`;
      } else {
        rgbaValue = `rgba(212,22,60,0.8)`;
      }
      //confirmed the rgba value, start drawing
      ctxOutput.beginPath();
      //draw each point as a rectangle
      ctxOutput.rect(i, j, 1, 1);
      ctxOutput.fillStyle = rgbaValue;
      ctxOutput.fill();
    }
  }

  //tranform data to png image and start downloading
  var download = function() {
    var link = document.createElement("a");
    link.download = "TMB_Analyzation.png";
    link.href = outputCanvas.toDataURL("image/png");
    link.click();
  };
  download();
  //tell popup.js report has been created
  chrome.runtime.sendMessage({ createdReport: true });
});
