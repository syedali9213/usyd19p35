//use background script to keep track of current domain
//if domain changes, recording will pause until manually resume
var currentHost;

//check if userID exists everytime user opens browser
//If not set a new userID
chrome.storage.local.get(['userId'], function(result) {
  if(result.userId === undefined){
    chrome.storage.local.set({ userId: generateUID()})
  }
})


//generate the UID
function generateUID() {
  // I generate the UID from two parts here 
  // to ensure the random number provide enough bits.
  var firstPart = (Math.random() * 46656) | 0;
  var secondPart = (Math.random() * 46656) | 0;
  firstPart = ("000" + firstPart.toString(36)).slice(-3);
  secondPart = ("000" + secondPart.toString(36)).slice(-3);
  return firstPart + secondPart;
}

//listen for url change event
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  //only execute when user is currently recording mouse movement
  chrome.storage.local.get(["recording"], function(result) {
    if (result.recording && changeInfo.status === "complete") {
      console.log('adding script to:', tab);
      chrome.tabs.executeScript(tabId, {
        file: "addEventListener.js"
      });
    }
  });
});

chrome.tabs.onActivated.addListener(function(activeInfo){
  chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
    var url = tabs[0].url;
});
})

chrome.runtime.onMessage.addListener(function(request) {
  if (request.currentHost) {
    currentHost = request.currentHost;
  }
});

//Listen for windows being opened -- fire when chrome is opened for the first time
chrome.windows.onCreated.addListener(function () {
  chrome.windows.getAll(function (windows) {
    if (windows.length == 1) {
      chrome.storage.local.set({ cursorMapVisible: false });
      chrome.storage.local.set({ scrollMapVisible: false });
      chrome.storage.local.set({ recording: false });
    }
  });
});

//Listen for extension installation/update -- reset to default settings
chrome.runtime.onInstalled.addListener(function() {
  chrome.storage.local.set({ cursorMapVisible: false });
  chrome.storage.local.set({ scrollMapVisible: false });
  chrome.storage.local.set({ recording: false });
});

function updateServer(){
  chrome.storage.local.get(["serverUpdateDict"], function(result){
    rawData = result.serverUpdateDict
    if(rawData !== "undefined" && rawData !== null){
      chrome.storage.local.get(["userId"], function(result){

          
        const url = 'http://localhost:3000/'
        var postObject = {};
        postObject["id"] = result.userId;
        postObject["data"] = rawData;

        //Post data to server using a post request
        fetch(url, {
            method : "POST",
            headers: {"Content-Type": "application/json"},
            // body: new FormData(document.getElementById("inputform")),
            // -- or --
            body : JSON.stringify(postObject),
        }).then(
            response => response.text() // .json(), etc.
        ).then(
            text => console.log(text)
        );
    }) 
    chrome.storage.local.set({serverUpdateDict: null})  
  } else {console.log('data is empty, not saving')}
  });
}

// 60000 = 1 minute
serverIntervalId = setInterval(updateServer, 5000);

