//get the intervalId stored in chrome local storage by 'addEventListener.js',
//clear the interval and that "recording" function will stop executing
chrome.storage.local.get(["intervalData"], function(result){
  var intervalId = result.intervalData[window.location.href]['intervalId']
  var getDataIntervalId = result.intervalData[window.location.href]['getDataIntervalId']
  var scrollIntervalId = result.intervalData[window.location.href]['scrollIntervalId']
  var statusIntervalId = result.intervalData[window.location.href]['statusIntervalId']
  clearInterval(intervalId);
  clearInterval(getDataIntervalId);
  clearInterval(scrollIntervalId);
  clearInterval(statusIntervalId)

})



// var intervalId;
// var getDataIntervalId;
// chrome.storage.local.get(["intervalId"], function(result) {
//   intervalId = result.intervalId;
// });
// clearInterval(intervalId);

// chrome.storage.local.get(["getDataIntervalId"], function(result) {
//   console.log('removing listener');
//   getDataIntervalId = result.getDataIntervalId;
// });
// clearInterval(getDataIntervalId);

// var scrollIntervalId;
// chrome.storage.local.get(["scrollIntervalId"], function(result) {
//   scrollIntervalId = result.scrollIntervalId;
// });
// clearInterval(scrollIntervalId);
