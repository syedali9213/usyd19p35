//List of states - NOTE only one of these should be "true" at a time
// - "recording" : Whether the application is currently recording or not
// - "cursorMapVisible" : Whether a request for the cursor heatmap has been placed
// - "scrollMapVisible" : Whether a request for the scroll heatmap has been placed

//Default states are returned in background.js, when the first chrome window is opened

// get all DOM element for later use
var startButton = document.getElementById("start");
var startIcon = document.getElementById("start-icon");
var pauseButton = document.getElementById("pause");
var pauseIcon = document.getElementById("pause-icon");
var clearButton = document.getElementById("clear");
var clearIcon = document.getElementById("clear-icon");
var clearSpan = document.getElementById("clear-span");
var reportButton = document.getElementById("report");
var reportIcon = document.getElementById("report-icon");
var reportSpan = document.getElementById("report-span");
var reportLoader = document.getElementById("report-loader");
var hideSpan = document.getElementById("hide-span");
var scrollButton = document.getElementById("scroll");
var scrollIcon = document.getElementById("scroll-icon");
var scrollSpan = document.getElementById("scroll-span");
var scrollLoader = document.getElementById("scroll-loader");
var saveDataButton = document.getElementById("save-data")

function update() {
  chrome.storage.local.get(["recording"], function(result) {
    if (result.recording === true) {
      startButton.style.display = "none";
      pauseButton.style.display = "block";
    } else {
      startButton.style.display = "block";
      pauseButton.style.display = "none";
    }
  });

  chrome.storage.local.get(["cursorMapVisible"], function(result2) {
    if (result2.cursorMapVisible === true) {
      reportIcon.style.display = "none";
      reportSpan.style.display = "none";
      reportLoader.style.display = "none";
      hideSpan.style.display = "block";
    } else {
      reportIcon.style.display = "block";
      reportSpan.style.display = "block";
      reportLoader.style.display = "none";
      hideSpan.style.display = "none";
    }
  });
}

update();