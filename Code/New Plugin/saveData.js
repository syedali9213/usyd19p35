var UID;
var rawData;



chrome.storage.local.get(["mouseMovementDict"], function(result) {

     //return if we don't have any data yet
    if (result.mouseMovementDict === null) {
        // chrome.runtime.sendMessage({ createdReport: true });
        alert("There are no data captured for this URL, please start recording");
        return;
    }

    // rawData = result.mouseMovementDict[window.location.href];

    rawData = result.mouseMovementDict;

    chrome.storage.local.get(["userId"], function(result){

        
        const url = 'http://localhost:3000/'
        var postObject = {};
        postObject["id"] = result.userId;
        postObject["data"] = rawData;

        //Post data to server using a post request
        fetch(url, {
            method : "POST",
            headers: {"Content-Type": "application/json"},
            // body: new FormData(document.getElementById("inputform")),
            // -- or --
            body : JSON.stringify(postObject),
        }).then(
            response => response.text() // .json(), etc.
            // same as function(response) {return response.text();}
        ).then(
            html => console.log(html)
        );
    })

});