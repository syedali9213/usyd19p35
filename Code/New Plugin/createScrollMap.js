// var canvasContainer = document.createElement('div');
// Add the div into the document
//  document.body.appendChild(canvasContainer);
//  canvasContainer.style.position="absolute";
//  // Set to 100% so that it will have the dimensions of the document
//  canvasContainer.style.left="0px";
//  canvasContainer.style.top="0px";
//  canvasContainer.style.width="100%";
//  canvasContainer.style.height="100%";
//  // Set to high index so that this is always above everything else
//  // (might need to be increased if you have other element at higher index)
//  canvasContainer.style.zIndex="1000";






chrome.storage.local.get(["scrollDict"], function(result) {
    var scrollArray = result.scrollDict[window.location.href]

    if(scrollArray === undefined) { console.log('returning'); return; }
    var canvCheck = document.getElementById('heatMapCanvas')
    if(canvCheck != null){
        canvCheck.remove();
        chrome.storage.local.set({ scrollMapVisible: false });
        return;
    }

    console.log(scrollArray.length)

    // Final Code working canvas
    var canv = document.createElement('canvas');
    var ctx = canv.getContext('2d')
    canv.id = 'heatMapCanvas'
    canv.style.position = 'absolute'
    canv.style.overflow = 'visible'
    canv.style.left="0px"
    canv.style.top="0px"
    canv.style.width=document.documentElement.scrollWidth+"px"
    canv.style.height=document.documentElement.scrollHeight+"px"
    canv.style.zIndex="2000"
    canv.style.opacity = 0.2
    canv.height = document.documentElement.scrollHeight;
    canv.width = document.documentElement.scrollWidth;

    var grd = ctx.createLinearGradient(0, 0, 0, canv.height)

    //normalise values for heatmap
    var min = Math.min(...scrollArray)
    var max = Math.max(...scrollArray)
    console.log(min, max)
    for(var i = 0; i < scrollArray.length; i++ ){
        let colorValue = heatMapColorforValue((scrollArray[i] - min) / (max - min))
        let normalisedIndex = ((i+1) - 0)/(scrollArray.length - 0)
        grd.addColorStop(normalisedIndex, colorValue)
    }
    // grd.addColorStop(0.2, 'red')
    // grd.addColorStop(.17777777777777778, "hsl(170.66666666666669, 100%, 50%)")
    ctx.fillStyle = grd
    ctx.fillRect(0, 0, canv.width, canv.height);
    // canvasContainer.appendChild(canv)
    document.body.appendChild(canv)
    chrome.storage.local.set({ scrollMapVisible: true });

});


function heatMapColorforValue(value){
    var h = (1.0 - value) * 240
    return "hsl(" + h + ", 100%, 50%)";
  }

