/**
 * @module Server:routes
 */

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const path = require('path');

const {startDatabase} = require('./database/mongo');
const {insertData, getData} = require('./database/data');
const {insertImageData, getImageData} = require('./database/imagetimedata');
const {insertTagData, getTagData} = require('./database/imagetags');

const vision = require('@google-cloud/vision')
const client = new vision.ImageAnnotatorClient()

const app = express();

// adding Helmet to enhance your API's security
app.use(helmet());
// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());
// enabling CORS for all requests
app.use(cors());
// adding morgan to log HTTP requests
app.use(morgan('combined'));

/**
 * Get all mouse and scroll data in the server
 * @function
 * @name GET/data
 * @param {json} res - Json response containing all data
 */
app.get('/', async (req, res) => {
    res.send(await getData());
})

/**
 * Post mouse and scroll data
 * @function
 * @name POST/data
 * @param {json} req - Request containing mouse and scrolling data
 * @param {json} res - Res confirming data has been inserted
 */
app.post('/', async (req, res) => {
    const newData = req.body;
    for(i in newData["data"]){
        await insertData(i, newData["data"][i], newData['id'])
    }
    // await insertData(newData);
    res.send({ message: 'New data inserted.' })
});

/**
 * Post timestamps with image urls
 * @function
 * @name POST/timestamps
 * @param {json} req - Contains timestamps and corresponding urls
 * @param {json} res - Response stating that data has been inserted and processed
 */
app.post('/timestamps', async (req, res) => {
    const newData = req.body;
    console.log(newData);
    if(req.body["id"] == null){
        res.send("userId is null please send a user id with request")
    }
    for(url in newData["data"]){
        await insertImageData(url, newData["data"][i], newData["id"])
        await insertTagData(url)
    }
    res.send("Image data added and processed")
})

/**
 * Get all the timestamps from the keyboard
 * @function
 * @name GET/timestamps
 * @param {json} req - Empty request object
 * @param {json} res - Response object containing all timestamps with corresponding URLs
 */
app.get('/timestamps', async(req, res) => {
    res.send(await getImageData())
})

/**
 * Get all image tags storerd in the database
 * @function
 * @name GET/tags
 * @param {json} req - Empty request object
 * @param {json} res - Response object containing all the tags and urls of images that have been sent to the server
 */
app.get('/tags', async(req, res) => {
    res.send(await getTagData())
})

/**
 * Start Database
 * @name startDatabase
 * @function
 */
startDatabase().then(async () => {
    /**
     * Start server and listen on port 3000
     * @function
     * @name listen
     * @param {int} port - Listening port for the server
     * @param {function} callback - function to execute after starting server
     */
    app.listen(3000, async () => {
      console.log('listening on port 3000');
    });
  });