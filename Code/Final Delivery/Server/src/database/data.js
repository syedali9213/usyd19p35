/**
 * @module Server:database:data
 */

const {getDatabase} = require('./mongo');
const {ObjectID} = require('mongodb');

const collectionName = 'mousedata';

/**
 * Insert mouse and scrolling data into database
 * @function
 * @name insertData
 * @param {string} url - URL of the website for which data is being inserted
 * @param {Array} data - Mouse and scroll data array
 * @param {string} userid - userid of the user posting the data
 */
async function insertData(url, data, userId) {
  const database = await getDatabase();

  //get count of current url occurence in user data
  count = await database.collection(collectionName).find({ _id: userId, "data.url": url }).count()

  //if current url has not been saved before create new entry for url
  //before saving data
  if(count === 0){
    await database.collection(collectionName).updateOne(
      {_id: userId},
      { $addToSet: {data: {url: url}}},
      { upsert: true }
    )
    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.mousedata": { $each: data } }},
      { upsert: true }
    );
    //if url already exists append data
  } else {

    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.mousedata": { $each: data } }}, 
      { upsert: true }
    );

  }
 
  return 1;
}

/**
 * Get and return mouse and scroll data from the database
 * @function
 * @name getData
 * @returns Json object containing mouse and scrolling data with corresponding urls
 */
async function getData() {
  const database = await getDatabase();
  return await database.collection(collectionName).find({}).toArray();
}


module.exports = {
  insertData,
  getData,
};