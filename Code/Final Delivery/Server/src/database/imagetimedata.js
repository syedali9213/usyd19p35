/**
 * @module server:database:timestamps
 */

const {getDatabase} = require('./mongo');
const {ObjectID} = require('mongodb');

const collectionName = 'imagetimedata';

/**
 * Insert timestamps for an image
 * @name insertImageData
 * @param {string} url - URL of the image
 * @param {Array} data - timestamp data for URL
 * @param {string} userId - userid of user for which data is being inserted
 */
async function insertImageData(url, data, userId) {
  const database = await getDatabase();

  //get number of times url occurs in database.
  //Should be 1 or 0
  count = await database.collection(collectionName).find({ _id: userId, "data.url": url }).count()
  console.log(count);
  //If url does not exist insert it and add timestamps
  if(count === 0){
    console.log('updating')
    await database.collection(collectionName).updateOne(
      {_id: userId},
      { $addToSet: {data: {url: url}}},
      { upsert: true }
    )
    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.imagedata": { $each: data } }},
      { upsert: true }
    );
    //if url already exits append data to that url
  } else {

    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.imagedata": { $each: data } }},
      { upsert: true }
    );

  }
 
  return 1;
}

/**
 * Get all image timestamps with corresponding URLs
 * @function
 * @name getImageData
 */
async function getImageData() {
  const database = await getDatabase();
  return await database.collection(collectionName).find({}).toArray();
}

module.exports = {
  insertImageData,
  getImageData,
  // deleteAd,
  // updateAd,
};