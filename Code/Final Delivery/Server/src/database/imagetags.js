/**
 * @module Server:database:tags
 */


const {getDatabase} = require('./mongo');
const {ObjectID} = require('mongodb');
const vision = require('@google-cloud/vision')

const client = new vision.ImageAnnotatorClient()

const collectionName = 'imagetags';

/**
 * Insert image tags by querying Google Vision API using the URL
 * @param {string} url - URL of image to get tags
 * @name insertTagData
 */
async function insertTagData(url) {
  const database = await getDatabase();

  //Get number of times current url occurs (Should be 1 or 0)
  count = await database.collection(collectionName).find({"_id": url }).count()

  //If tags for current url do not exist in database
  if(count === 0){
    
    //get results of url from google vision api
    const [result] = await client.labelDetection(url)
    //extract labels from resutls
    const labels = result.labelAnnotations
    //add tags to tags array
    var tags = []
    labels.forEach(label => {
        tags.push(label.description)
    });

    //insert tags into database
    database.collection(collectionName).insertOne(
        {
            "_id": url,
            "tags": tags
        }
      );

  }
  return 1;
}

/**
 * Retrieve all tags with their corresponding URLs
 * @function
 * @name getTagData
 */
async function getTagData() {
  const database = await getDatabase();
  return await database.collection(collectionName).find({}).toArray();
}

module.exports = {
  insertTagData,
  getTagData,
  // deleteAd,
  // updateAd,
};