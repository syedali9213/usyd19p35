// get all DOM element for later use
var startButton = document.getElementById("start");
var startIcon = document.getElementById("start-icon");
var pauseButton = document.getElementById("pause");
var pauseIcon = document.getElementById("pause-icon");
var clearButton = document.getElementById("clear");
var clearIcon = document.getElementById("clear-icon");
var clearSpan = document.getElementById("clear-span");
var reportButton = document.getElementById("report");
var reportIcon = document.getElementById("report-icon");
var reportSpan = document.getElementById("report-span");
var reportLoader = document.getElementById("report-loader");
var hideSpan = document.getElementById("hide-span");
var scrollButton = document.getElementById("scroll");
var scrollIcon = document.getElementById("scroll-icon");
var scrollSpan = document.getElementById("scroll-span");
var scrollLoader = document.getElementById("scroll-loader");
var testButton = document.getElementById("test");
// var saveDataButton = document.getElementById("save-data")

// Style "none" means hide, Style "block" means show

function runTests() {
    ////////////////////////////////////////////////////////////////
    // TEST CASES SET 1
    ////////////////////////////////////////////////////////////////

    // Test 1.1 - Play button showing
    if (startButton.style.display == "block") document.getElementById("1.1").style.backgroundColor = "lightgreen";
    else document.getElementById("1.1").style.backgroundColor = "red";

    // Test 1.2 - Pause button hidden
    if (pauseButton.style.display == "block") document.getElementById("1.2").style.backgroundColor = "red";
    else document.getElementById("1.2").style.backgroundColor = "lightgreen";

    // Test 1.3 - Clear data button clickable
    if (document.getElementById("clear").disabled) document.getElementById("1.3").style.backgroundColor = "red";
    else document.getElementById("1.3").style.backgroundColor = "lightgreen";

    // Test 1.4 - Generate cursor map button showing
    if (reportSpan.style.display == "block") document.getElementById("1.4").style.backgroundColor = "lightgreen";
    else document.getElementById("1.4").style.backgroundColor = "red";

    // Test 1.5 - Hide cursor map button hidden
    if (hideSpan.style.display == "block") document.getElementById("1.5").style.backgroundColor = "red";
    else document.getElementById("1.5").style.backgroundColor = "lightgreen";

    // Test 1.6 - Should not be recording
    chrome.storage.local.get(["recording"], function(result) {
        if (result.recording) document.getElementById("1.6").style.backgroundColor = "red";
        else document.getElementById("1.6").style.backgroundColor = "lightgreen";
    });

    // Test 1.7 - Cursor heatmap not shown
    chrome.storage.local.get(["cursorMapVisible"], function(result) {
        if (result.cursorMapVisible) document.getElementById("1.7").style.backgroundColor = "red";
        else document.getElementById("1.7").style.backgroundColor = "lightgreen";
    });

    // Test 1.8 - Scroll heatmap not shown
    chrome.storage.local.get(["scrollMapVisible"], function(result) {
        if (result.scrollMapVisible) document.getElementById("1.8").style.backgroundColor = "red";
        else document.getElementById("1.8").style.backgroundColor = "lightgreen";
    });

    // Test 1.9 - Heatmap loader not shown
    if (scrollLoader.style.display == "block") document.getElementById("1.9").style.backgroundColor = "red";
    else document.getElementById("1.9").style.backgroundColor = "lightgreen";

    ////////////////////////////////////////////////////////////////
    // TEST CASES SET 2
    ////////////////////////////////////////////////////////////////

    // Running the function to start recording - the same attached to play button in popup.js
    addMousemovementListener();

    // Test 2.1 - Play button hidden
    if (startButton.style.display == "block") document.getElementById("2.1").style.backgroundColor = "red";
    else document.getElementById("2.1").style.backgroundColor = "lightgreen";

    // Test 2.2 - Pause button shown
    if (pauseButton.style.display == "block") document.getElementById("2.2").style.backgroundColor = "lightgreen";
    else document.getElementById("2.2").style.backgroundColor = "red";

    // Test 2.3 - Clear data button unclickable
    if (document.getElementById("clear").disabled) document.getElementById("2.3").style.backgroundColor = "lightgreen";
    else document.getElementById("2.3").style.backgroundColor = "red";

    // Test 2.4 - Generate cursor map button showing
    if (reportSpan.style.display == "block") document.getElementById("2.4").style.backgroundColor = "lightgreen";
    else document.getElementById("2.4").style.backgroundColor = "red";

    // Test 2.5 - Hide cursor map button hidden
    if (hideSpan.style.display == "block") document.getElementById("2.5").style.backgroundColor = "red";
    else document.getElementById("2.5").style.backgroundColor = "lightgreen";

    // Test 2.6 - Should be recording
    chrome.storage.local.get(["recording"], function(result) {
        if (result.recording) document.getElementById("2.6").style.backgroundColor = "lightgreen";
        else document.getElementById("2.6").style.backgroundColor = "red";
    });

    // Test 2.7 - Cursor heatmap not shown
    chrome.storage.local.get(["cursorMapVisible"], function(result) {
        if (result.cursorMapVisible) document.getElementById("2.7").style.backgroundColor = "red";
        else document.getElementById("2.7").style.backgroundColor = "lightgreen";
    });

    // Test 2.8 - Scroll heatmap not shown
    chrome.storage.local.get(["scrollMapVisible"], function(result) {
        if (result.scrollMapVisible) document.getElementById("2.8").style.backgroundColor = "red";
        else document.getElementById("2.8").style.backgroundColor = "lightgreen";
    });
    
    // Test 2.9 - Heatmap loader not shown
    if (scrollLoader.style.display == "block") document.getElementById("2.9").style.backgroundColor = "red";
    else document.getElementById("2.9").style.backgroundColor = "lightgreen";
    
    ////////////////////////////////////////////////////////////////
    // TEST CASES SET 3
    ////////////////////////////////////////////////////////////////
    
    // Pause before proceeding
    removeMousemovementListener(); 

    // Test 3.1 - Play button showing
    if (startButton.style.display == "block") document.getElementById("3.1").style.backgroundColor = "lightgreen";
    else document.getElementById("3.1").style.backgroundColor = "red";

    // Test 3.2 - Pause button hidden
    if (pauseButton.style.display == "block") document.getElementById("3.2").style.backgroundColor = "red";
    else document.getElementById("3.2").style.backgroundColor = "lightgreen";

    // Test 3.3 - Clear data button clickable
    if (document.getElementById("clear").disabled) document.getElementById("3.3").style.backgroundColor = "red";
    else document.getElementById("3.3").style.backgroundColor = "lightgreen";

    // Test 3.4 - Generate cursor map button showing
    if (reportSpan.style.display == "block") document.getElementById("3.4").style.backgroundColor = "lightgreen";
    else document.getElementById("3.4").style.backgroundColor = "red";

    // Test 3.5 - Hide cursor map button hidden
    if (hideSpan.style.display == "block") document.getElementById("3.5").style.backgroundColor = "red";
    else document.getElementById("3.5").style.backgroundColor = "lightgreen";

    // Test 3.6 - Should not be recording
    chrome.storage.local.get(["recording"], function(result) {
        if (result.recording) document.getElementById("3.6").style.backgroundColor = "red";
        else document.getElementById("3.6").style.backgroundColor = "lightgreen";
    });

    // Test 3.7 - Cursor heatmap not shown
    chrome.storage.local.get(["cursorMapVisible"], function(result) {
        if (result.cursorMapVisible) document.getElementById("3.7").style.backgroundColor = "red";
        else document.getElementById("3.7").style.backgroundColor = "lightgreen";
    });

    // Test 3.8 - Scroll heatmap not shown
    chrome.storage.local.get(["scrollMapVisible"], function(result) {
        if (result.scrollMapVisible) document.getElementById("3.8").style.backgroundColor = "red";
        else document.getElementById("3.8").style.backgroundColor = "lightgreen";
    });
   
    // Test 3.9 - Heatmap loader not shown
    if (scrollLoader.style.display == "block") document.getElementById("3.9").style.backgroundColor = "red";
    else document.getElementById("3.9").style.backgroundColor = "lightgreen";

    ////////////////////////////////////////////////////////////////
    // TEST CASES SET 4
    ////////////////////////////////////////////////////////////////

    // Press the cursor map button
    createReport();

    // Test 4.1 - Play button showing
    if (startButton.style.display == "block") document.getElementById("4.1").style.backgroundColor = "lightgreen";
    else document.getElementById("4.1").style.backgroundColor = "red";

    // Test 4.2 - Pause button hidden
    if (pauseButton.style.display == "block") document.getElementById("4.2").style.backgroundColor = "red";
    else document.getElementById("4.2").style.backgroundColor = "lightgreen";

    // Test 4.3 - Clear data button clickable
    if (document.getElementById("clear").disabled) document.getElementById("4.3").style.backgroundColor = "lightgreen";
    else document.getElementById("4.3").style.backgroundColor = "red";

    // Test 4.4 - Generate cursor map button showing
    if (reportSpan.style.display == "block") document.getElementById("4.4").style.backgroundColor = "lightgreen";
    else document.getElementById("4.4").style.backgroundColor = "red";

    // Test 4.5 - Hide cursor map button hidden
    if (hideSpan.style.display == "block") document.getElementById("4.5").style.backgroundColor = "red";
    else document.getElementById("4.5").style.backgroundColor = "lightgreen";

    // Test 4.6 - Should not be recording
    chrome.storage.local.get(["recording"], function(result) {
        if (result.recording) document.getElementById("4.6").style.backgroundColor = "red";
        else document.getElementById("4.6").style.backgroundColor = "lightgreen";
    });

    // Test 4.7 - Cursor heatmap not shown
    chrome.storage.local.get(["cursorMapVisible"], function(result) {
        if (result.cursorMapVisible) document.getElementById("4.7").style.backgroundColor = "red";
        else document.getElementById("4.7").style.backgroundColor = "lightgreen";
    });

    // Test 4.8 - Scroll heatmap not shown
    chrome.storage.local.get(["scrollMapVisible"], function(result) {
        if (result.scrollMapVisible) document.getElementById("4.8").style.backgroundColor = "red";
        else document.getElementById("4.8").style.backgroundColor = "lightgreen";
    });

    // Test 4.9 - Heatmap loader not shown
    if (scrollLoader.style.display == "block") document.getElementById("4.9").style.backgroundColor = "red";
    else document.getElementById("4.9").style.backgroundColor = "lightgreen";
}

testButton.addEventListener("click", runTests);