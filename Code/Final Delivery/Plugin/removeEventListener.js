//get the intervalId stored in chrome local storage by 'addEventListener.js',
//clear the interval and that "recording" function will stop executing
console.log("removing listener");
chrome.storage.local.get(["intervalData"], function(result){
  var intervalId = result.intervalData[window.location.href]['intervalId']
  var getDataIntervalId = result.intervalData[window.location.href]['getDataIntervalId']
  var scrollIntervalId = result.intervalData[window.location.href]['scrollIntervalId']
  var statusIntervalId = result.intervalData[window.location.href]['statusIntervalId']
  var imageSrcIntervalId = result.intervalData[window.location.href]['imageSrcIntervalId']
  clearInterval(intervalId);
  clearInterval(getDataIntervalId);
  clearInterval(scrollIntervalId);
  clearInterval(imageSrcIntervalId)
  clearInterval(statusIntervalId)

})