const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const path = require('path');

const {startDatabase} = require('./database/mongo');
const {insertData, getData} = require('./database/data');
const {insertImageData, getImageData} = require('./database/imagetimedata');
const {insertTagData, getTagData} = require('./database/imagetags');

const vision = require('@google-cloud/vision')
const client = new vision.ImageAnnotatorClient()

const app = express();

// adding Helmet to enhance your API's security
app.use(helmet());
// using bodyParser to parse JSON bodies into JS objects
app.use(bodyParser.json());
// enabling CORS for all requests
app.use(cors());
// adding morgan to log HTTP requests
app.use(morgan('combined'));

//define endpoint
app.get('/', async (req, res) => {
    res.send(await getData());
})

//endpoint to add a new add. Each request has keys id and data
app.post('/', async (req, res) => {
    const newData = req.body;
    for(i in newData["data"]){
        await insertData(i, newData["data"][i], newData['id'])
    }
    // await insertData(newData);
    res.send({ message: 'New data inserted.' })
});

app.post('/imagedata', async (req, res) => {
    const newData = req.body;
    console.log(newData);
    if(req.body["id"] == null){
        res.send("userId is null please send a user id with request")
    }
    for(i in newData["data"]){
        await insertImageData(i, newData["data"][i], newData["id"])
        await insertTagData(i)
    }
    res.send("Image data added and processed")
})

app.get('/imagedata', async(req, res) => {
    res.send(await getImageData())
})

app.get('/tags', async(req, res) => {
    res.send(await getTagData())
})

app.post('/test', async(req, res) => {
    const imageURL = req.body["url"]
    console.log(imageURL);
    const [result] = await client.labelDetection(imageURL)
    const labels = result.labelAnnotations
    console.log('Labels:')
    labels.forEach(label => console.log(label.description))
    res.send("Image has been assigned a thing")
})


app.get('/index', async(req, res) => {
    res.sendFile(path.join(__dirname + '/views/index.html'));
})

startDatabase().then(async () => {
    // await insertAd({title: 'Hello, now from the in-memory database!'});
  
    // start the server
    app.listen(3000, async () => {
      console.log('listening on port 3000');
    });
  });