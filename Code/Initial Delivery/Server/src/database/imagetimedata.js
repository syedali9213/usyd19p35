const {getDatabase} = require('./mongo');
const {ObjectID} = require('mongodb');

const collectionName = 'imagetimedata';

async function insertImageData(url, data, userId) {
  const database = await getDatabase();
  console.log("userId:", userId)

  count = await database.collection(collectionName).find({ _id: userId, "data.url": url }).count()
  console.log(count);

  if(count === 0){
    console.log('updating')
    await database.collection(collectionName).updateOne(
      {_id: userId},
      { $addToSet: {data: {url: url}}},
      { upsert: true }
    )
    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.imagedata": { $each: data } }},
      { upsert: true }
    );

  } else {

    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.imagedata": { $each: data } }},
      { upsert: true }
    );

  }
 
  return 1;
}

async function getImageData() {
  const database = await getDatabase();
  return await database.collection(collectionName).find({}).toArray();
}

module.exports = {
  insertImageData,
  getImageData,
  // deleteAd,
  // updateAd,
};