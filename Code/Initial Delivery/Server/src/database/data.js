const {getDatabase} = require('./mongo');
const {ObjectID} = require('mongodb');

const collectionName = 'mousedata';

async function insertData(url, data, userId) {
  const database = await getDatabase();
  console.log("userId:", userId)

  count = await database.collection(collectionName).find({ _id: userId, "data.url": url }).count()

  if(count === 0){
    await database.collection(collectionName).updateOne(
      {_id: userId},
      { $addToSet: {data: {url: url}}},
      { upsert: true }
    )
    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.mousedata": { $each: data } }},
      { upsert: true }
    );

  } else {

    database.collection(collectionName).updateOne(
      { _id: userId, "data.url": url },
      { $push: { "data.$.mousedata": { $each: data } }}, 
      { upsert: true }
    );

  }
 

 // {id: [{url: []}, ]}

  // const {insertedId} = await database.collection(collectionName).insertOne(t);
  return 1;
}

async function getData() {
  const database = await getDatabase();
  return await database.collection(collectionName).find({}).toArray();
}

// async function deleteAd(id) {
//     const database = await getDatabase();
//     await database.collection(collectionName).deleteOne({
//       _id: new ObjectID(id),
//     });
//   }

// async function updateAd(id, ad) {
// const database = await getDatabase();
// delete ad._id;
// await database.collection(collectionName).update(
//     { _id: new ObjectID(id), },
//     {
//     $set: {
//         ...ad,
//     },
//     },
// );
// }

module.exports = {
  insertData,
  getData,
  // deleteAd,
  // updateAd,
};