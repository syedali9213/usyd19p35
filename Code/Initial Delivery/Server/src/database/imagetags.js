const {getDatabase} = require('./mongo');
const {ObjectID} = require('mongodb');
const vision = require('@google-cloud/vision')

const client = new vision.ImageAnnotatorClient()

const collectionName = 'imagetags';

async function insertTagData(url) {
  const database = await getDatabase();

  count = await database.collection(collectionName).find({"_id": url }).count()

  if(count === 0){
    
    const [result] = await client.labelDetection(url)
    const labels = result.labelAnnotations
    var tags = []
    labels.forEach(label => {
        tags.push(label.description)
    });

    database.collection(collectionName).insertOne(
        {
            "_id": url,
            "tags": tags
        }
      );

  }
  return 1;
}

async function getTagData() {
  const database = await getDatabase();
  return await database.collection(collectionName).find({}).toArray();
}

module.exports = {
  insertTagData,
  getTagData,
  // deleteAd,
  // updateAd,
};