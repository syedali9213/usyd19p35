chrome.storage.local.get(["scrollDict"], function(result) {
    var scrollArray = result.scrollDict[window.location.href]

    if(scrollArray === undefined) { console.log('returning'); return; }
    var canvCheck = document.getElementById('heatMapCanvas')
    if(canvCheck != null){
        canvCheck.remove();
        return;
    }

    console.log(scrollArray.length)

    // Final Code working canvas, creating the scroll map canvas
    var canv = document.createElement('canvas');
    var ctx = canv.getContext('2d')
    canv.id = 'heatMapCanvas'
    canv.style.position = 'absolute'
    canv.style.overflow = 'visible'
    canv.style.left="0px"
    canv.style.top="0px"
    canv.style.width=document.documentElement.scrollWidth+"px"
    canv.style.height=document.documentElement.scrollHeight+"px"
    canv.style.zIndex="2000"
    canv.style.opacity = 0.2
    canv.height = document.documentElement.scrollHeight;
    canv.width = document.documentElement.scrollWidth;

    var grd = ctx.createLinearGradient(0, 0, 0, canv.height)

    //normalise values for heatmap, this helps differentiate the colour and intensity of the heatmap
    var min = Math.min(...scrollArray)
    var max = Math.max(...scrollArray)
    console.log(min, max)
    for(var i = 0; i < scrollArray.length; i++ ){
        let colorValue = heatMapColorforValue((scrollArray[i] - min) / (max - min))
        let normalisedIndex = ((i+1) - 0)/(scrollArray.length - 0)
        grd.addColorStop(normalisedIndex, colorValue)
    }
    ctx.fillStyle = grd
    ctx.fillRect(0, 0, canv.width, canv.height);
    // canvasContainer.appendChild(canv)
    document.body.appendChild(canv)

});

//calculates the colour required for displaying the corresponding intensity
function heatMapColorforValue(value){
    var h = (1.0 - value) * 240
    return "hsl(" + h + ", 100%, 50%)";
  }

