//define two variables to store mouse position
var positionX;
var positionY;

//define two variables to store the current window position
var windowPositionX;
var windowPositionY;

//ids of some variables to keep track of
var IntervalId;
var getDataIntervalId;
var scrollIntervalId;
var statusIntervalId;

//listen mouse event
document.onmousemove = function(e) {
  //get the mouse position in fully rendered content area in the browser
  windowPositionX = e.clientX;
  windowPositionY = e.clientY;
  var x = e.clientX + pageXOffset;
  var y = e.clientY + pageYOffset;
  positionX = x > document.documentElement.scrollWidth ? document.documentElement.scrollWidth : x;
  positionY = y > document.documentElement.scrollHeight ? document.documentElement.scrollHeight : y;
};

//use following flag to track whether mouse is currently in the content area,
//will pause recording if mouse leaves this area and resume when it comes back
if(document.hidden) var mouseIsInBrowsingArea = false;
else var mouseIsInBrowsingArea = true
document.onmouseenter = function(e) {
  mouseIsInBrowsingArea = true;
};
document.onmouseleave = function(e) {
  mouseIsInBrowsingArea = false;
};

function getElementData(){
  var imgs = document.elementFromPoint(windowPositionX, windowPositionY).getElementsByTagName('img')
  var d = new Date().getTime()
  if(imgs.length == 1){
    var src = imgs[0].src
    console.log(src);
    if(src != undefined){
      //imageDict stores image urls and timestamps for when a user hovered over an image
      chrome.storage.local.get(["imageDict"], function(result){
        //check if the imageURL dictionary exists
        if(
          typeof result.imageDict !== "undefined" &&
          result.imageDict instanceof Object
        ) {
          if(!result.imageDict[src]){
            result.imageDict[src] = [d]
          } else {
            result.imageDict[src].push(d)
          }

        } else {
          result.imageDict = {}
          result.imageDict[src] = [d]
        }

        chrome.storage.local.set({imageDict: result.imageDict})
      })
    }
  }
}
  

//store mouse position in chrome local storage as an array
function getNewMousePosition() {
  //don't record if mouse is not in content area
  if (!mouseIsInBrowsingArea || !positionX || !positionY) {
    return;
  }
  //represent current mouse position as an array with a length of 2
  // console.log(positionX, Math.round(positionY));
  var newPointArray = [positionX, Math.round(positionY)];
  //will store all positions we get in chrome local storage as key value pair,
  //key is the url of the website, value is a two dimensional array,
  //each time we get a position, we push it into this big array for the corresponding url
  //if it is the very first position we've got, we will need to create this array
  chrome.storage.local.get(["mouseMovementDict"], function(result) {
    //check if the dictionary exists
    if (
      typeof result.mouseMovementDict !== "undefined" &&
      result.mouseMovementDict instanceof Object
    ) {
      //check if the current key exists
      if(!result.mouseMovementDict[window.location.href]){
        //this is the first point for this url, create a new array
        result.mouseMovementDict[window.location.href] = [newPointArray]
      } else {
        //already data for this url, push to existing array
        result.mouseMovementDict[window.location.href].push(newPointArray);
      }
    } else {
      //this is the first point we got, put it in an array and assign it to result.mouseMovementDict
      result.mouseMovementDict = {};
      result.mouseMovementDict[window.location.href] = [newPointArray]
    }
    //store the modified array back to chrome local storage
    chrome.storage.local.set({ mouseMovementDict: result.mouseMovementDict });
  });

  chrome.storage.local.get(["serverUpdateDict"], function(result) {
    //check if the dictionary exists
    if (
      typeof result.serverUpdateDict !== "undefined" &&
      result.serverUpdateDict instanceof Object
    ) {
      //check if the current key exists
      if(!result.serverUpdateDict[window.location.href]){
        //this is the first point for this url, create a new array
        result.serverUpdateDict[window.location.href] = [newPointArray]
      } else {
        //already data for this url, push to existing array
        result.serverUpdateDict[window.location.href].push(newPointArray);
      }
    } else {
      //this is the first point we got, put it in an array and assign it to result.serverUpdateDict
      result.serverUpdateDict = {};
      result.serverUpdateDict[window.location.href] = [newPointArray]
    }
    //store the modified array back to chrome local storage
    chrome.storage.local.set({ serverUpdateDict: result.serverUpdateDict });
  });
}

function getScrollPosition() {
  // //don't record if mouse is not in content area
  if (!mouseIsInBrowsingArea || !positionX || !positionY) {
    return;
  }
  chrome.storage.local.get(["scrollDict"], function(result){
    if(
      //instantiate object if it exists
      typeof result.scrollDict !== "undefined" && 
      result.scrollDict instanceof Object
    ) {

      if(!result.scrollDict[window.location.href]){
        //this is the first point for this url, create a new array to store data (coordinates)
        result.scrollDict[window.location.href] = []
        for(var i = 0; i < Math.ceil(document.documentElement.scrollHeight/100) ; i++) result.scrollDict[window.location.href][i] = 0;
        let top = Math.floor(window.pageYOffset/100);
        let bottom = top + Math.ceil(document.documentElement.clientHeight/100);
        for(var i = top; i < bottom; i++) result.scrollDict[window.location.href][i]++

      } else {
        //already data for this url, push to existing array
        let top = Math.floor(window.pageYOffset/100);
        let bottom = top + Math.ceil(document.documentElement.clientHeight/100);
        for(var i = top; i < bottom; i++) result.scrollDict[window.location.href][i]++        
      }
      
    } else {
      result.scrollDict = {}
      result.scrollDict[window.location.href] = [];
      for(var i = 0; i < Math.ceil(document.documentElement.scrollHeight/100) ; i++) result.scrollDict[window.location.href][i] = 0;
      let top = Math.floor(window.pageYOffset/100);
      let bottom = top + Math.ceil(document.documentElement.clientHeight/100);
      for(var i = top; i < bottom; i++) result.scrollDict[window.location.href][i]++
    }

    chrome.storage.local.set({ scrollDict: result.scrollDict });

  })

  chrome.storage.local.get(["serverScrollDict"], function(result){
    if(
      //instantiate object if it exists
      typeof result.serverScrollDict !== "undefined" && 
      result.serverScrollDict instanceof Object
    ) {

      if(!result.serverScrollDict[window.location.href]){
        //this is the first point for this url, create a new array to store data (coordinates)
        result.serverScrollDict[window.location.href] = []
        for(var i = 0; i < Math.ceil(document.documentElement.scrollHeight/100) ; i++) result.serverScrollDict[window.location.href][i] = 0;
        let top = Math.floor(window.pageYOffset/100);
        let bottom = top + Math.ceil(document.documentElement.clientHeight/100);
        for(var i = top; i < bottom; i++) result.serverScrollDict[window.location.href][i]++

      } else {
        //already data for this url, push to existing array
        let top = Math.floor(window.pageYOffset/100);
        let bottom = top + Math.ceil(document.documentElement.clientHeight/100);
        for(var i = top; i < bottom; i++) result.scrollDict[window.location.href][i]++        
      }
      
    } else {
      result.serverScrollDict = {}
      result.serverScrollDict[window.location.href] = [];
      for(var i = 0; i < Math.ceil(document.documentElement.scrollHeight/100) ; i++) result.serverScrollDict[window.location.href][i] = 0;
      let top = Math.floor(window.pageYOffset/100);
      let bottom = top + Math.ceil(document.documentElement.clientHeight/100);
      for(var i = top; i < bottom; i++) result.serverScrollDict[window.location.href][i]++
    }

    chrome.storage.local.set({ serverScrollDict: result.serverScrollDict });

  })
}

function checkState() {
  chrome.storage.local.get(["recording"], function(result) {
    if (result.recording) {
      //Do nothing
    }
    //if it's no longer recording, remove everything
    else {
      clearInterval(intervalId);
      clearInterval(getDataIntervalId);
      clearInterval(scrollIntervalId);
      clearInterval(imageSrcIntervalId)
      clearInterval(statusIntervalId);
      console.log('removing listener (changed domain/tab)');
    }
  });
}

//set an interval of 0.1s to continuously execute this function to get positions
intervalId = setInterval(getNewMousePosition, 100);
getDataIntervalId = setInterval(getElementData, 1000);
scrollIntervalId = setInterval(getScrollPosition, 100)
statusIntervalId = setInterval(checkState, 1000);
imageSrcIntervalId = setInterval(getElementData, 2000)

//get ids and store them
chrome.storage.local.get(["intervalData"], function(result){
  if (
    typeof result.intervalData !== "undefined" &&
    result.intervalData instanceof Object
  ){
    result.intervalData[window.location.href] = {
      intervalId: intervalId,
      getDataIntervalId: getDataIntervalId,
      scrollIntervalId: scrollIntervalId,
      statusIntervalId: statusIntervalId,
      imageSrcIntervalId: imageSrcIntervalId
    }

  } else {
    result.intervalData = {};
    result.intervalData[window.location.href] = {
      intervalId: intervalId,
      getDataIntervalId: getDataIntervalId,
      scrollIntervalId: scrollIntervalId,
      statusIntervalId: statusIntervalId,
      imageSrcIntervalId: imageSrcIntervalId
    }

  }

  chrome.storage.local.set({ intervalData: result.intervalData });
})
