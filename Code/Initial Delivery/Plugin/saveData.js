var UID;
var rawData;

console.log("Saving data");
console.log(new Date());



chrome.storage.local.get(["serverUpdateDict"], function(result) {

     //return if we don't have any data yet
    if (result.mouseMovementDict === null) {
        alert("There are no data captured for this URL, please start recording");
        return;
    }


    rawData = result.serverUpdateDict;

    chrome.storage.local.get(["userId"], function(result){

        //save id and data into our site defined by the url
        const url = 'http://localhost:3000/'
        var postObject = {};
        postObject["id"] = result.userId;
        postObject["data"] = rawData;

        //Post data to server using a post request
        fetch(url, {
            method : "POST",
            headers: {"Content-Type": "application/json"},

            body : JSON.stringify(postObject),
        }).then(
            response => response.text()
        ).then(
            html => console.log(html)
        );
    })

});

chrome.storage.local.set({ serverUpdateDict: null});



var userId = null
var data = null
chrome.storage.local.get(['userId'], function(result){
    if(result.userId !== undefined){
    userId = result.userId
    chrome.storage.local.get(['imageDict'], function(result){
        data = result.imageDict
        console.log(data)
        //save id and data into our server defined by the url
        const url = 'http://localhost:3000/imagedata';
        var postObject = {};
        postObject["id"] = userId;
        postObject["data"] = data;

        //Post data to server using a post request
        fetch(url, {
            method : "POST",
            headers: {"Content-Type": "application/json"},

            body : JSON.stringify(postObject),
        }).then(
            response => response.text()
        )

    })
    }
})

// chrome.storage.local.set({imageDict: null})
