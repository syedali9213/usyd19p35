//use background script to keep track of current domain
//if domain changes, recording will pause until manually resume
var currentHost;

//check if userID exists everytime user opens browser
//If not set a new userID
chrome.storage.local.get(['userId'], function(result) {
  if(result.userId === undefined){
    chrome.storage.local.set({ userId: generateUID()})
  }
})

//Interval to pass image urls to the server for tag allocation and analysis
// function saveData(){
//   var userId = null
//   var data = null
//   chrome.storage.local.get(['userId'], function(result){
//     if(result.userId !== undefined){
//       userId = result.userId
//       chrome.storage.local.get(['imageDict'], function(result){
//         data = result.imageDict
//         //save id and data into our server defined by the url
//         const url = 'http://localhost:3000/classifyImages';
//         var postObject = {};
//         postObject["id"] = result.userId;
//         postObject["data"] = rawData;

//         //Post data to server using a post request
//         fetch(url, {
//             method : "POST",
//             headers: {"Content-Type": "application/json"},

//             body : JSON.stringify(postObject),
//         }).then(
//             response => response.text()
//         ).then(
//             html => console.log(html)
//         );

//       })
//     }
//   })
// }

// var saveDataIntervalId = setInterval(saveData, 60000)

//generate the UID
function generateUID() {
  // I generate the UID from two parts here 
  // to ensure the random number provide enough bits.
  var firstPart = (Math.random() * 46656) | 0;
  var secondPart = (Math.random() * 46656) | 0;
  firstPart = ("000" + firstPart.toString(36)).slice(-3);
  secondPart = ("000" + secondPart.toString(36)).slice(-3);
  return firstPart + secondPart;
}

//listen for url change event
chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
  //only execute when user is currently recording mouse movement
  chrome.storage.local.get(["recording"], function(result) {
    if (result.recording && changeInfo.status === "complete") {
      console.log('adding script to:', tab);
      chrome.tabs.executeScript(tabId, {
        file: "addEventListener.js"
      });
    }
  });
});

//Identify that tab is in use
chrome.tabs.onActivated.addListener(function(activeInfo){
  chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function (tabs) {
    var url = tabs[0].url;
});
})

chrome.runtime.onMessage.addListener(function(request) {
  if (request.currentHost) {
    currentHost = request.currentHost;
  }
});
