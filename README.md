# Cursor Tracking 2.0

By Project Group 4 (Project 35) consisting of:

- Syed Ali
- Jack Chen
- Nengneng Guo
- Arjun Suresh

---

### [1] Introduction

This chrome extension accepts user cursor and scroll positions as input
and is able to output the relative positions and time spent at each position
in the form of a heatmap.

The extension is able to produce two types of heatmaps:

- Cursor heatmap
- Scroll heatmap

The heatmaps have a scale that can be interpreted as follows:

- No colour: No time spent at location
- Blue: Little, but not zero spent time at location
- Green: Moderate relative time spent at location
- Red: Most relative time spent at location

_____________________________________________________________________________

### [2] How to install application

- Open a window in Google Chrome
- Navigate to chrome://extensions
- Click "Load Unpacked"
- Navigate to the folder containing javascript code and select it
- Application is now installed

---

### [3] How to load server

- Navigate to Server folder
- In the command line type in `export GOOGLE_APPLICATION_CREDENTIALS="cp35-key.json"`
- From the command line run the server by typing `nodemon src/index.js`
- The server runs on port 3000 by default

_____________________________________________________________________________

### [4] How to generate heatmaps

#### [4.1] Cursor heatmap

- Click the black mouse icon at the top right of the screen
- Click the Play button to start recording mouse input
- Move the cursor around on the current web page as normal
- Click the Pause button to stop recording mouse input
- Click "View Cursor Map" - heatmap will be shown as coloured circles
- Click "Hide heatmap" to hide output

#### [4.2] Scroll heatmap

- Ensure that a page where scrolling is possible is open
- Click the black mouse icon at the top right of the screen
- Click the Play button to start recording scroll input
- Use the scroll wheel as normal on the web page
- Click the Pause button to stop recording scroll input
- Click "View Scroll Map" - heatmap will be shown as coloured bars
- Click "View Scroll Map" again to hide output

### [5] Get Data

- To get mouse and scroll data send a `GET` request to http://localhost:3000/data
- To get timestamp data send a `GET` to http://localhost:3000/timestamps
- To get tag data send a `GET` request to http://localhost:3000/tags