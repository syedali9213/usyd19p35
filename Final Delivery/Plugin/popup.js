/**
 * @module Plugin:Popup
 */


// get all DOM element for later use
var startButton = document.getElementById("start");
var startIcon = document.getElementById("start-icon");
var pauseButton = document.getElementById("pause");
var pauseIcon = document.getElementById("pause-icon");
var clearButton = document.getElementById("clear");
var clearIcon = document.getElementById("clear-icon");
var clearSpan = document.getElementById("clear-span");
var reportButton = document.getElementById("report");
var reportIcon = document.getElementById("report-icon");
var reportSpan = document.getElementById("report-span");
var reportLoader = document.getElementById("report-loader");
var hideSpan = document.getElementById("hide-span");
var scrollButton = document.getElementById("scroll");
var scrollIcon = document.getElementById("scroll-icon");
var scrollSpan = document.getElementById("scroll-span");
var scrollLoader = document.getElementById("scroll-loader");
var saveDataButton = document.getElementById("save");



/**
 * Inject content script for tracking user
 * @function
 * @name addMousemovementListener
 */
function addMousemovementListener() {
  //hide start button, show pause button
  startButton.style.display = "none";
  pauseButton.style.display = "block";
  //disable clear button
  document.getElementById("clear").disabled=true;
  //keep recording status (currrently recording or not) in the chrome local storage,
  //if not, this status will be lost when popup window is closed
  chrome.storage.local.set({ recording: true });
  //tell background script to update current domain
  chrome.tabs.getSelected(function(tab) {
    var url = new URL(tab.url);
    chrome.runtime.sendMessage({ currentHost: url.hostname });
  });

  //execute main recording function
  chrome.tabs.getAllInWindow(null, function(tabs){
    for (var i = 0; i < tabs.length; i++) {
    if(tabs[i].status === "complete"){
      chrome.tabs.executeScript(tabs[i].id, { file: "addEventListener.js" });                         
    }
    }
  });
}

/**
 * stop recording function
 * @function
 * @name removeMousemovementListener
 */
function removeMousemovementListener() {
  //hide pause button, show start button
  startButton.style.display = "block";
  pauseButton.style.display = "none";
  //enable clear button
  document.getElementById("clear").disabled=false;
  //change recording status to "stop"
  chrome.storage.local.set({ recording: false });

  chrome.tabs.getAllInWindow(null, function(tabs){
    for (var i = 0; i < tabs.length; i++) {
    if(tabs[i].status === "complete"){
      chrome.tabs.executeScript(tabs[i].id, { file: "removeEventListener.js" });                         
    }
    }
  });
}

/**
 * clear data from local storage function
 * @function
 * @name clearData
 */
function clearData() {
  //stop recording in case user forgot to stop it
  chrome.storage.local.get(["recording"], function(result) {
    if (result.recording === true) {
      removeMousemovementListener();
    }
  });
  //clear recorded data stored in chrome local storage
  chrome.tabs.executeScript({
    file: "clearData.js"
  });
}

/**
 * Generate cursor heatmap
 * @function
 * @name createdReport
 */
function createReport() {
  //show loader
  reportIcon.style.display = "none";
  reportSpan.style.display = "none";
  reportLoader.style.display = "block";

  //stop recording in case user forgot to stop it
  chrome.storage.local.get(["recording"], function(result) {
    if (result.recording === true) {
      removeMousemovementListener();
    }
  });

  //create report
  chrome.tabs.executeScript({
    file: "createReport.js"
  });
}

/**
 * generate and display scroll map to the user
 * @function
 * @name createScrollMap
 */
function createScrollMap() {
  //stop recording in case user forgot to stop it
  chrome.storage.local.get(["recording"], function(result) {
    if (result.recording === true) {
      removeMousemovementListener();
    }
  });

  //create scroll map
  chrome.tabs.executeScript({
    file: "createScrollMap.js"
  });
}

function saveData() {
  chrome.tabs.executeScript({
    file: "saveData.js"
  })
}
//when popup window loads, update the HTML to reflect current status
var currentHost;

//add eventlisteners for click event on all four buttons, executed above functions accordingly
startButton.addEventListener("click", addMousemovementListener);
pauseButton.addEventListener("click", removeMousemovementListener);
clearButton.addEventListener("click", clearData);
reportButton.addEventListener("click", createReport);
scrollButton.addEventListener("click", createScrollMap);
saveDataButton.addEventListener("click", saveData)


//create report script will send a message when it has completed the task,
//we can hide the loader upon receiving this message
chrome.runtime.onMessage.addListener(function(request) {
  if (request.createdReport) {
    reportIcon.style.display = "block";
    reportLoader.style.display = "none";
    hideSpan.style.display = "block"
  }

  if (request.hiddenReport) {
    reportIcon.style.display = "block";
    reportSpan.style.display = "block";
    reportLoader.style.display = "none";
    hideSpan.style.display = "none"
  }
});
